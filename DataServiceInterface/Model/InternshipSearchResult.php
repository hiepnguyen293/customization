<?php
namespace Ngay4\Internship\Model;

use Magento\Framework\Api\SearchResults;
use Ngay4\Internship\Api\Data\InternshipSearchResultInterface;

class InternshipSearchResult extends SearchResults implements InternshipSearchResultInterface
{

}
