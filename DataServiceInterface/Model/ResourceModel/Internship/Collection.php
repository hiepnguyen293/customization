<?php

namespace Ngay4\Internship\Model\ResourceModel\Internship;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Custom method
     */
    protected function _construct()
    {
        $this->_init(
            'Ngay4\Internship\Model\Internship',
            'Ngay4\Internship\Model\ResourceModel\Internship'
        );
    }
}
