<?php
namespace Ngay4\Internship\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface InternshipSearchResultInterface extends SearchResultsInterface
{
    /*
     * @return \Ngay4\Internship\Api\Data\InternshipInterface[]
     */
    public function getItems();

    /*
     * @param \Ngay4\Internship\Api\Data\InternshipInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}
