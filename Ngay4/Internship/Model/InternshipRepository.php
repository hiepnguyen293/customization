<?php

namespace Ngay4\Internship\Model;

use Ngay4\Internship\Api\InternshipRepositoryInterface;
use Ngay4\Internship\Api\Data\InternshipInterface;
use Ngay4\Internship\Api\Data\InternshipSearchResultInterface;
use Ngay4\Internship\Api\Data\InternshipSearchResultInterfaceFactory;
use Ngay4\Internship\Model\ResourceModel\Internship as ResourceInternship;
use Ngay4\Internship\Model\ResourceModel\Internship\CollectionFactory as InternshipCollectionFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

class InternshipRepository implements InternshipRepositoryInterface
{
    protected $resource;
    protected $internshipFactory;
    protected $internshipCollectionFactory;
    /**
     * @var InternshipSearchResultInterface
     */
    private $searchResultFactory;
    protected $collectionProcessor;
    protected $instance = [];

    public function __construct(
        ResourceInternship $resource,
        InternshipFactory $internshipFactory,
        InternshipCollectionFactory $internshipCollectionFactory,
        InternshipSearchResultInterfaceFactory $searchResultFactory,
        CollectionProcessorInterface $collectionProcessor
    )
    {
        $this->resource = $resource;
        $this->internshipFactory = $internshipFactory;
        $this->internshipCollectionFactory = $internshipCollectionFactory;
        $this->searchResultFactory = $searchResultFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    public function save(InternshipInterface $internship)
    {
        try {
            $this->resource->save($internship);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        unset($this->instances[$internship->getId()]);
        return $internship;
    }

    public function getById($internshipId)
    {
        if (!isset($this->instances[$internshipId])) {
            $internship = $this->internshipFactory->create();
            $this->resource->load($internship, $internshipId);
            if (!$internship->getId()) {
                throw new NoSuchEntityException(__('internship with id "%1" does not exist.', $internshipId));
            }
            $this->instances[$internshipId] = $internship;
        }

        return $this->instances[$internshipId];
    }

    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, ($collection));
        $searchResults = $this->searchResultFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }

    public function delete(InternshipInterface $internship)
    {
        try {
            $internshipId = $internship->getId();
            $this->resource->delete($internship);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        unset($this->instances[$internshipId]);
        return true;
    }


}
