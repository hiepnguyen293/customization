<?php

namespace Ngay4\Internship\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Internship extends AbstractDb
{
    /**
     * Custom method
     */
    public function _construct()
    {
        $this->_init('internship1', 'id');
    }
}
