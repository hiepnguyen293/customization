<?php

namespace Ngay4\Internship\Model;

use Ngay4\Internship\Api\Data\InternshipInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;

class Internship extends AbstractModel implements InternshipInterface, IdentityInterface
{
    const CACHE_TAG = 'Ngay4_Internship';

    protected $_cacheTag = self::CACHE_TAG;

    protected $_eventPrefix = self::CACHE_TAG;

    protected function _construct()
    {
        $this->_init(\Ngay4\Internship\Model\ResourceModel\Internship::class);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getId()
    {
        return $this->getData(self::INTERNSHIP_ID);
    }

    public function setId($id)
    {
        return $this->setData(self::INTERNSHIP_ID,$id);
    }

    public function getName()
    {
        return $this->getData(self::INTERNSHIP_NAME);
    }

    public function setName($name)
    {
        return $this->getData(self::INTERNSHIP_NAME,$name);
    }

    public function getDob()
    {
        return $this->getData(self::INTERNSHIP_DOB);
    }

    public function setDob($dob)
    {
        return $this->getData(self::INTERNSHIP_DOB,$dob);
    }

    public function getAvatar()
    {
        return $this->getData(self::INTERNSHIP_AVATAR);
    }

    public function setAvatar($avatar)
    {
        return $this->getData(self::INTERNSHIP_AVATAR,$avatar);
    }

    public function getDescription()
    {
        return $this->getData(self::INTERNSHIP_DESCRIPTION);
    }

    public function setDescription($description)
    {
        return $this->getData(self::INTERNSHIP_DESCRIPTION,$description);
    }
}
