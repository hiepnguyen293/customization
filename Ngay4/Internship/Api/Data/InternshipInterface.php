<?php
namespace Ngay4\Internship\Api\Data;

interface InternshipInterface
{
    const INTERNSHIP_ID = 'id';
    const INTERNSHIP_NAME = 'name';
    const INTERNSHIP_DOB = 'dob';
    const INTERNSHIP_AVATAR = 'avatar';
    const INTERNSHIP_DESCRIPTION = 'description';

    /*
        *  get/set Id
    */
    public function getId();
    public function setId($id);

    /*
        *  get/set name
    */
    public function getName();
    public function setName($name);

    /*
        *  get/set dob
    */
    public function getDob();
    public function setDob($dob);

    /*
        *  get/set avatar
    */
    public function getAvatar();
    public function setAvatar($avatar);

    /*
        *  get/set description
    */
    public function getDescription();
    public function setDescription($description);

}
