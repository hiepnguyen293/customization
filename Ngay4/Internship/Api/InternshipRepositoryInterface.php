<?php

namespace Ngay4\Internship\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface InternshipRepositoryInterface
{
    /**
     * Save internship
     *
     * @param \Ngay4\Internship\Api\Data\InternshipInterface $internship
     * @return \Ngay4\Internship\Api\Data\InternshipInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\InternshipInterface $internship);

    /**
     * Retrieve internship
     *
     * @param int $internshipId
     * @return \Ngay4\Internship\Api\Data\InternshipInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($internshipId);

    /**
     * Retrieve internship matching the specified criteria
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Ngay4\Internship\Api\Data\InternshipSearchResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete internship
     *
     * @param \Ngay4\Internship\Api\Data\InternshipInterface $internship
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\InternshipInterface $internship);

}
