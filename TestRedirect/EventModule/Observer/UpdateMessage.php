<?php

namespace TestRedirect\EventModule\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class UpdateMessage implements ObserverInterface
{
    /** @var \Magento\Framework\Message\ManagerInterface */
    protected $messageManager;
    /** @var \Magento\CatalogInventory\Api\StockStateInterface */
    protected $_stockItem;

    public function __construct(
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\Message\ManagerInterface $managerInterface,
        \Magento\CatalogInventory\Api\StockStateInterface $stockItem
    ) {
        $this->cart = $cart;
        $this->messageManager = $managerInterface;
        $this->_stockItem = $stockItem;
    }

    public function execute(Observer $observer)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $stockItem = $objectManager->get('\Magento\CatalogInventory\Model\Stock\StockItemRepository');
        $item = $observer->getEvent()->getData('quote_item');
        $itemStockId = $item->getProductId();
        $productStock = $stockItem->get($itemStockId)->getQty();
        $productName = $item->getName();
        $messageCollection = $this->messageManager->getMessages(true);
        $customMessage = 'Added to cart Product ID ' . $itemStockId . ' | ' . 'Name of Product ' . $productName . ' | ' . ' Current Stock Quantity ' . $productStock;
        $this->messageManager->addSuccess($customMessage);

    }
}
